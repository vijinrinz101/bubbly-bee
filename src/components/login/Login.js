import SelectLogin from '../index/SelectLogin'
import LearnerLogin from './LearnerLogin';
import EarnerLogin from './EarnerLogin'
import OtpVerification from './OtpVerification';
import { UserContext } from '../../context/UserContext';
import { useContext, useState } from 'react';
import { Col, Row, Button, Card, Input, Form, Select, Image } from 'antd';
import './login.css'
import bee from './BEE.png'


const Login = () => {
    const { loginType, setLoginType } = useContext(UserContext)


    const [mainCategory, setMainCategory] =  useState('earn') 
    

    

    return (
        <div className="login-page">
            {/* {loginType === 'selectLogin' && <SelectLogin />}
            {loginType === 'learnerLogin' && 
            <LearnerLogin />}


            {loginType === 'earnerLogin' && <EarnerLogin />} */}

            <div className="abc">
                <Row className="d-flex" >
                    <Col span={8} className="d-flex flex-column justify-content-between  py-5 ps-5 login-bg" style={{ height: "100vh" }}>
                        <div>
                            <div>
                                <img className="img-fluid" src="https://college-images-kumari.s3.ap-south-1.amazonaws.com/bubblybee+images/logo.png" width="150px"></img>
                            </div>
                            <div className="heading">
                                <h1 className="text-light">Earn while you learn </h1>
                                <h1 className="text-light">Learn while you earn</h1>
                            </div>
                            <div>
                                <p className="text-light">
                                    We support each individual accordinly in order to fulfill the requirements for the complete development of abilities and talents
                                </p>
                            </div>

                        </div>

                        <div>
                            <div>
                                <select className="border-0 " style={{ backgroundColor: "transparent", color: "gray" }}>
                                    <option>INDIA</option>
                                </select>
                            </div>
                        </div>

                    </Col>
                    <div className=" bee-img">
                        <Image
                            width={200}
                            src={bee}
                            preview={false}
                        />

                    </div>
                    <Col span={16} className="d-flex justify-content-center align-items-center col2">
                        <Card>
                           
                           
                            {mainCategory === 'earn' &&  <EarnerLogin  setMainCategory={setMainCategory} />}
                            { mainCategory === 'learn'  &&   <LearnerLogin setMainCategory={setMainCategory} />}
                            { mainCategory === 'otp'  &&   <OtpVerification setMainCategory={setMainCategory} />}
                        </Card>
                    </Col>
                </Row>
            </div>

        </div>
    );
}

export default Login;