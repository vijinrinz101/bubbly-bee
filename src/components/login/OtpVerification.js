import {Form, Button, Input} from "antd";

const OtpVerification = () => {
    return ( 
        <div style={{width:"500px"}}>
        <Form>
        <Form.Item

name="otp"
rules={[
  {
    required: true,
    message: 'Please enter your OTP',
  },
]}
>
<Input type="number" placeholder="Enter OTP" />
</Form.Item>
<Form.Item className="text-center">
                <Button type="primary" htmlType="submit" className="login-form-button">
                  Confirm Account
                </Button>
              </Form.Item>
        </Form>
        </div>
     );
}
 
export default OtpVerification;