import { Form, Input, Button, Select} from 'antd';
import { useState } from 'react';

const LearnerLogin = ({ setMainCategory}) => {

  const { Option } = Select;
  const [category, setCategory] =  useState('login-earn') 
  const onFinish = (values) => {
    console.log('Success:', values);
};
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
};
const prefixSelector = (
    <Form.Item name="prefix" noStyle>
        <Select showArrow={false}
            style={{
                width: 70,
            }}

            defaultValue='91'
        >
            <Option value="91">+91</Option>

        </Select>
    </Form.Item>
);
  

  return (
    <div>
      <div className="text-center">
                                <Button style={{borderRadius:"10px"}} size="large" className="me-5" onClick={() => {   setMainCategory('earn') }}>Bubbly Bee earn Login</Button>
                                <Button size="large" type="primary"   onClick={() => { setCategory('login-earn'); setMainCategory('learn') }}>{category === 'login-earn' ? "Bubbly Bee learn Login" :  "Bubbly Bee learn Signup"}</Button>
                            </div>
      {category === 'login-earn' && <div>
        <div className="pt-4">
          <h4>Bubbly Bee Learn Login</h4>
        </div>
        <div>
          <Form
            name="basic"
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 16,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="phone"

              rules={[
                {
                  required: true,
                  message: 'Please input your phone number!',
                },
              ]}
            >
              <Input
              type="number"
                placeholder="Phone Number"
                addonBefore={prefixSelector}
                style={{
                  width: '',
                }}
              />
            </Form.Item>


            <Form.Item

              name="otp"
              rules={[
                {
                  required: true,
                  message: 'Please enter your OTP',
                },
              ]}
            >
              <Input placeholder="Enter OTP" type="number" />
            </Form.Item>
            <Form.Item className="text-center">
              <Button type="primary" htmlType="submit" className="login-form-button">
                Login
              </Button>
            </Form.Item>
          </Form>
          <div>
            Don't have an account? <a onClick={() => { setCategory('register-earn') }}>Signup</a>
          </div>
        </div></div>}


      {category === 'register-earn' &&
        <div>
          <div className="pt-4">
            <h4>Bubbly Bee Learn Signup</h4>
          </div>
          <div>
            <Form
              name="basic"
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name={['user', 'email']}

                rules={[
                  {
                    type: 'email',
                  },
                ]}
              >
                <Input placeholder="Enter Email" />
              </Form.Item>
              <Form.Item
                name="phone"

                rules={[
                  {
                    required: true,
                    message: 'Please input your phone number!',
                  },
                ]}
              >
                <Input
                type="number"
                  placeholder="Phone Number"
                  addonBefore={prefixSelector}
                  style={{
                    width: '',
                  }}
                />
              </Form.Item>


              <Form.Item

                name="otp"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input type="number" placeholder="Enter OTP" />
              </Form.Item>
              <Form.Item name="pincode" rules={[
                {
                  required: true,
                  message: 'Enter Pincode',
                },
              ]}>
                <Input
                  type="number"
                  placeholder="Enter Pincode"
                />
              </Form.Item>
              <Form.Item className="text-center">
                <Button type="primary" htmlType="submit" className="login-form-button">
                  Create Account
                </Button>
              </Form.Item>



            </Form>
            <div>
              Already have an account?                <a onClick={() => { setCategory('login-earn') }}>Login</a>
            </div>
          </div>
        </div>}
    </div>

  );
}

export default LearnerLogin;