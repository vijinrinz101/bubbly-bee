import { Form, Input, Button, Select} from 'antd';
import { useEffect, useState } from 'react';
import { useTimer } from 'react-timer-hook';

function MyTimer({ expiryTimestamp }) {
  const [temp,setTemp]=useState(false)
  const {
    seconds,
    minutes,
    hours,
    days,
     isRunning,
    start,
    pause,
    resume,
    restart,
  } = useTimer({ expiryTimestamp, onExpire: () => console.warn('onExpire called') });

  const [otp, setOtp] = useState(false)
useEffect(()=>{
if(isRunning===false){
  setOtp(false)
}
},[isRunning])
  return (
    <div style={{textAlign: 'center'}}>
     
      <div >
        {console.log(isRunning)}
        {otp===true&&<><span>{minutes}</span>:<span>{seconds}</span></>}
        {otp === false &&  <Button style={{width:"100%", border:"none" , fontSize:"10px"}} onClick={() => {
        // Restarts to 5 minutes timer
        setTemp(true);
        setOtp(true);
        const time = new Date();
        time.setSeconds(time.getSeconds()+30);
        restart(time)
      }}>get otp</Button>}
      </div>
     
    </div>
  );
}

const EarnerLogin = ({ setMainCategory}) => {

  const { Option } = Select;
  const [category, setCategory] =  useState('login-earn') 

  const time = useState("")

  const onFinish = (values) => {

    var raw = JSON.stringify({"username":values.user.email.substring(0, values.user.email.indexOf('@')),"email":values.user.email,"password":values.password, "phone":values.prefix===undefined?"+91".concat(values.phone):[], "type":"EARN"});
    var requestOptions = {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: raw,
      redirect: 'follow'
      };

  fetch("https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev/app/bubble-bee/signup/", requestOptions)
  .then(response => response.json())
  .then(result => {
      if(result.statusCode===200){
console.log(result);
setMainCategory('otp')
      }})
  
    
};
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
};
const prefixSelector = (
    <Form.Item name="prefix" noStyle>
        <Select showArrow={false}
            style={{
                width: 70,
            }}

            defaultValue='+91'
        >
            <Option value="+91">+91</Option>

        </Select>
    </Form.Item>
);

const sufixSelector = (
  <Form.Item name="prefix" noStyle>
     <div>
      <MyTimer  expiryTimestamp={time} />
    </div>
  </Form.Item>
);

  

  return (
    
    <div>
      
      <div className="text-center">
                                <Button type="primary" size="large" className="me-5" onClick={() => {   setMainCategory('earn') }}>{category === 'login-earn' ? "Bubbly Bee Earn Login" :  "Bubbly Bee Earn Signup"}</Button>
                                <Button size="large" style={{borderRadius:"10px"}}  onClick={() => { setCategory('login-earn'); setMainCategory('learn') }}>Bubbly Bee learn Login</Button>
                            </div>
      {category === 'login-earn' && <div>
        <div className="pt-4">
          <h4>Bubbly Bee Earn Login</h4>
        </div>
        <div>
          <Form
            name="basic"
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 16,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="phone"

              rules={[
                {
                  required: true,
                  message: 'Please enter your phone number!',
                },
              ]}
            >
              <Input
              
              type="number"
                placeholder="Phone Number"
                addonBefore={prefixSelector}
                
                style={{
                  width: '',
                }}
               
              />
            </Form.Item>


            <Form.Item

              name="otp"
              rules={[
                {
                  required: true,
                  message: 'Please enter your OTP',
                },
              ]}
            >
              <Input type="number" placeholder="Enter OTP" />
            </Form.Item>
            <Form.Item className="text-center">
              <Button type="primary" htmlType="submit" className="login-form-button">
                Login
              </Button>
            </Form.Item>
          </Form>
          <div>
            Don't have an account? <a onClick={() => { setCategory('register-earn') }}>Signup</a>
          </div>
        </div></div>}


      {category === 'register-earn' &&
        <div>
          <div className="pt-4">
            <h4>Bubbly Bee Earn Signup</h4>
          </div>
          <div>
            <Form
              name="basic"
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name={['user', 'email']}

                rules={[
                  {
                    type: 'email',
                    message: 'Enter your valid email'
                  },
                ]}
              >
                <Input  placeholder="Enter Email" />
              </Form.Item>
              <Form.Item
                name="phone"

                rules={[
                  {
                    required: true,
                    message: 'Please input your phone number!',
                  },
                ]}
              >
                <Input
                type="number"
                  placeholder="Phone Number"
                  addonBefore={prefixSelector}
                 
                  style={{
                    width: '',
                  }}
                />
              </Form.Item>


              
              <Form.Item
        name="password"
        
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
        hasFeedback
      >
        <Input.Password placeholder="Password" style={{border:"none"}} />
      </Form.Item>

      <Form.Item
        name="confirm"
        
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input.Password placeholder="Confirm password" style={{border:"none"}} />
      </Form.Item>
            
      <Form.Item className="text-center">
                <Button type="primary" htmlType="submit" className="login-form-button">
                  Create Account
                </Button>
              </Form.Item>


            </Form>
            <div>
              Already have an account?                <a onClick={() => { setCategory('login-earn') }}>Login</a>
            </div>
          </div>
        </div>}
    </div>

  );
}

export default EarnerLogin;