import { Button } from 'antd';
import { useContext, useState } from 'react';
import '../index/SelectionLogin.css'
import { UserContext } from '../../context/UserContext';

const SelectLogin = () => {

    const {loginType,setLoginType} = useContext(UserContext)

    return ( 
        <div className="section1">
            <div className="hi d-flex py-5 justify-content-center align-items-center bg-dark gap-3">
            <Button type="primary" size="large" className="" onClick={()=>{setLoginType('earnerLogin')}}>Earner Login</Button>
             <Button size="large" type="primary" onClick={()=>{setLoginType('learnerLogin')}}>Learner Login</Button>
            </div>
            
        </div>
     );
}
 
export default SelectLogin;