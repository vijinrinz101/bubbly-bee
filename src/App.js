import 'antd/dist/antd.css';
import './App.css';
import UserDetailsContextProvider from './context/UserContext';

import Login from './components/login/Login';


function App() {
  return (
    <div className="App">
      <UserDetailsContextProvider>
      <Login />
      </UserDetailsContextProvider>
    </div>
  );
}

export default App;
