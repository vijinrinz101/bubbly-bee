import React, { useEffect, useState, createContext } from 'react';


export const UserContext = createContext();
export default function UserDetailsContextProvider({children}){
    const [loginType, setLoginType] = useState('selectLogin')

    return (
        <UserContext.Provider value={{loginType,setLoginType}}>
            {children}
        </UserContext.Provider>
    )
}

 



